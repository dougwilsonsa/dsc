<#
  This script clones an existing vhd in azure to a new file.
  
  It makes the following assumptions:
  
  * You have 2 containers on your storage account, one named "vhds" (this should be there by default anyway) that stores the source .vhd files, 
    another named "clones" (you'll have to make this) that will store the destination vhds.
  * You treat your "vhds" folder as your "Gold Master" and never turn those machines on (except to update of course)
  * You use the "clones" folder for the disks powering your running instances
  
  Please note:
  
  * This script deliberately copies the entire file. If we are going to work this way after LnP then we should probably make use of the snapshot features.

#>

$subscriptionName = ""
$storageAccountName = ""
$resourceGroup = ""
$storageAccountType = ""
$storageAccountKey = "**************************"
$vhdName = "OriginalMachine.vhd"
$newFileName = "NewMachine.vhd" 
$vhdContainer = "vhds"
$destinationContainer = "clones"

Login-AzureRmAccount
Select-AzureRmSubscription -SubscriptionName $subscriptionName

Set-AzureRmStorageAccount -StorageAccountName $storageAccountName -ResourceGroupName $resourceGroup -Type $storageAccountType
$context = New-AzureStorageContext -StorageAccountName $storageAccountName -StorageAccountKey $storageAccountKey
$vhd = Get-AzureStorageBlob -Container $vhdContainer -Context $context | Where-Object { $_.Name -eq $vhdName }
Write-Output "Found " $vhd.Name
$unixDate = Get-Date -UFormat %s

Write-Output "New file " $newFileName
Write-Output "Copying..." 
Start-AzureStorageBlobCopy -Context $context -ICloudBlob $vhd.ICloudBlob -DestBlob $newFileName -DestContainer $destinationContainer
Write-Output "Done"
