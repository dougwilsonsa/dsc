<#
  This script clones an existing vhd in azure to a new file.
  
  It makes the following assumptions:
  
  * You have 2 containers on your storage account, one named "vhds" (this should be there by default anyway) that stores the source .vhd files, 
    another named "clones" (you'll have to make this) that will store the destination vhds.
  * You treat your "vhds" folder as your "Gold Master" and never turn those machines on (except to update of course)
  * You use the "clones" folder for the disks powering your running instances
  
  Please note:
  
  * This script deliberately copies the entire file. If we are going to work this way after LnP then we should probably make use of the snapshot features.

#>

$AzureDataCenter = "West Europe"
$subscriptionName = ""
$storageAccountName = ""
$resourceGroup = ""
<#

  For storageAccountType your options are:
  -- Standard_LRS. Locally-redundant storage. 
  -- Standard_ZRS. Zone-redundant storage.
  -- Standard_GRS. Geo-redundant storage. 
  -- Standard_RAGRS. Read access geo-redundant storage. 
  -- Premium_LRS. Premium locally-redundant storage.
#>
$storageAccountType = "Standard_LRS"
$storageAccountKey = "**************"
$vhdName = "BaseMachine.vhd" 
$virtualNetworkName = ""
$subnetName = ""
$newVmName = "MyVM2"
$vmSize = "Basic_A0"  # Get list from : Get-AzureRmVMSize -Location "West Europe"

Login-AzureRmAccount
Select-AzureRmSubscription -SubscriptionName $subscriptionName
Set-AzureRmStorageAccount -StorageAccountName $storageAccountName -ResourceGroupName $resourceGroup -Type $storageAccountType
$context = New-AzureStorageContext -StorageAccountName $storageAccountName -StorageAccountKey $storageAccountKey
$vhd = Get-AzureStorageBlob -Container "vhds" -Context $context | Where-Object { $_.Name -eq $vhdName }
Write-Output "Found " $vhd.Name
$unixDate = Get-Date -UFormat %s
$newFileName = $vhd.Name + "_" + $unixDate + ".vhd"
Write-Output "New file " $newFileName
Write-Output "Copying..." 
Start-AzureStorageBlobCopy -Context $context -ICloudBlob $vhd.ICloudBlob -DestBlob $newFileName -DestContainer "clones"

Write-Output "Creating new VM"
$network = Get-AzureRmVirtualNetwork -ResourceGroupName $resourceGroup -Name $virtualNetworkName | Select Subnets
$subnet = $network.Subnets | Where-Object { $_.Name -eq $subnetName }

$nicName = $newVmName + "_NIC"
$pip = New-AzureRmPublicIpAddress -Name $nicName -ResourceGroupName $resourceGroup -Location $AzureDataCenter -AllocationMethod Dynamic
$nic = New-AzureRmNetworkInterface -Name $nicName -ResourceGroupName $resourceGroup -Location $AzureDataCenter -SubnetId $subnet.Id -PublicIpAddressId $pip.Id

$vm = New-AzureRmVMConfig -VMName $newVmName -VMSize $vmSize 
$osDiskVhdUri = $context.BlobEndPoint + "clones/" + $newFileName
$osDiskName = $vhd.Name + "_osDisk"
$vm = Set-AzureRmVMOSDisk -VM $vm -VhdUri $osDiskVhdUri -name $osDiskName -CreateOption attach -Windows -Caching "ReadWrite"
$vm = Add-AzureRmVMNetworkInterface -VM $vm -Id $nic.Id

New-AzureRmVM -ResourceGroupName $resourceGroup -Location $AzureDataCenter -VM $vm
Write-Output "Done"