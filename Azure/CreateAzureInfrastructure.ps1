 <#
  .SYNOPSIS
  Generates infrastructure in Azure.
  
  .DESCRIPTION
  Generates infrastructure in an Azure data center based on configuration in a provided json file. 
  
  .PARAMETER PathToConfigFile
  The path to the json configuration file that describes the desired infrastructure. 
  
  .EXAMPLE
  CreateAzureInfrastructure C:\MyProject\azureConfig.json
  
  .EXAMPLE
  CreateAzureInfrastructure -PathToConfigFile C:\MyProject\azureConfig.json
  
  .EXAMPLE
  Sample config file
  
  { 
    "account" : { //All account/billing settings
      "subscriptionName":"Visual Studio Enterprise with MSDN", //The name of the Azure subscription to use.
      "resourceGroupName": "MySystemResourceGroup", //The name of the Azure Resource Group - will be created if it does not already exist.
      "dataCenterName": "West Europe", //The Azure Location that the infrastructure will be generated in. Choose from the list of Azure Data Centers in the portal.
      "storage": {
        "accountName": "MyStorageAccount", //The name of the storage account. Can be anything.
        "type": "Standard_LRS" //The type of storage account. Valid values are "Standard_LRS" (Locally-redundant storage), "Standard_ZRS" (Zone redundant storage), "Standard_GRS" (Geo-redundant storage), "Standard_RAGRS" (Read Access geo-redundant storage), "Premium_LRS".
      }
    },
    "certificates": [{ //list of certificate files
      "certificateName" : "MyCert", //name given to the certificate. Can be anything.
      "certificateFilePath" : "C:\\PathToCertificates\\certificate-name.cer" //path to the certificate file. Create cert instructions : https://azure.microsoft.com/en-us/documentation/articles/vpn-gateway-certificates-point-to-site/
    }],
    "infrastructure" : { //Infrastructure to be generated. 
      "virtualNetworks" : [{ //list of Virtual Networks to create
        "networkName" : "MyCompanyNetwork", //The name of the Virtual Network
        "ipV4addressPrefixes": [ "192.168.0.0/16", "10.254.0.0/16" ], //The address ranges for the virtual network
        "subnets" : [{ //list of subnets to add to the network
          "subnetName" : "ToolsSubnet", //name of subnet.  Can be anything.
          "ipV4addressPrefix" : "192.168.1.0/24"  //subnet address range
        },{
          "subnetName" : "DMZSubnet",
          "ipV4addressPrefix" : "10.254.1.128/24",
          "virtualMachines": [{
			      "name": "webserver1",  //server host name. Can be anything.  
            "networkInterface": {
              "nicName": "vmnic", //name of the network adapter. Can be anything.
              "assignPublicIp": true, //assign this machine an internet facing IP.
              "ipAddress": "10.254.1.130", //local private static IP address. Must be on the subnet. 
			        "publicDnsName": "www.likeaboss.com" //public internet facing dns name. Not required.
            },
            "size": "Basic_A0", //choose from Get-AzureRmVMSize. Understand at https://azure.microsoft.com/en-us/pricing/details/virtual-machines/#windows
            "image": { //as per info found in Azure portal
              "publisherName" : "MicrosoftWindowsServer",
              "offer" : "WindowsServer",
              "skuName" : "2012-R2-Datacenter"
            },
            "localAdminCredentials" : {
              "username" : "MyCompanyAdmin", //the username for the local administrator account. Can be anything.
              "passwordEncryptedString" : "" //The secure string of the password, created by running: $PWord = ConvertTo-SecureString –String "mySecretPassword" –AsPlainText -Force
            }
          }] 
        },{
          "subnetName" : "BackendSubnet",
          "ipV4addressPrefix" : "10.254.1.0/24" 
        },{ 
          "subnetName" : "GateWaySubnet",
          "ipV4addressPrefix" : "10.254.1.0/24",
          "gateway": { //gateway and VPN settings
            "gatewayName" : "VirtualNetworkNetworkGateway", //name of the gateway. Can be anything.
            "ipConfigurationName" : "GatewayIP", //IP address name.
            "gatewayType" : "Vpn", //Type of gateway to create. Valid values are "Vpn" or "ExpressRoute".
            "vpnType" : "RouteBased", //Type of VPN to create. Valid values are "RouteBased" or "PolicyBased".
            "enableBGP" : false, //Enable or disable Border Gateway Protocol
            "gatewaySKU" : "Standard", //Billing Stock Keeping Unit. Valid values are "Basic", "Standard" or "HighPerformance".
            "vpnClientAddressPool" : "172.16.201.0/24", //Network range to assign IP addresses to VPN client when they connect.
            "vpnRootCertificateName": "MyCert" //The name of the certificate to use as the root certificate for VPN connections. Must match a certificate name in the certificates collection on this object.
          } 
        }],
        "dnsServer": "8.8.8.8" //The IP Address or Hostname of the dns server to use. 8.8.8.8 is Google.
      }]
    }
  }  
#>
param
(
    [string]$PathToConfigFile
    
)

Import-Module AzureRM.Storage
Set-Location $PSScriptRoot

Function Ensure-ResourceGroup
{
  param
  (
    [string]$GroupName,
    [string]$Location
  )
  
  $rg = Get-AzureRmResourceGroup -Name $GroupName
  If (-Not($rg))
  {
    Write-Host "Creating Resource Group " $GroupName
    $rg = New-AzureRmResourceGroup -Name $GroupName -Location $Location
  }
  return $rg
}

Function Ensure-StorageAccount
{
  param
  (
    [string]$StorageAccountName,
    [string]$StorageAccountType,
    [string]$ResourceGroupName,
    [string]$Location
  )
  
  Write-Host $ResourceGroupName $StorageAccountName $StorageAccountType
  $SA = Get-AzureRmStorageAccount -ResourceGroupName $ResourceGroupName -Name $StorageAccountName
  If (-Not($SA))
  {
    $SA = New-AzureRmStorageAccount -ResourceGroupName $ResourceGroupName -Location $Location -Name $StorageAccountName -Type $StorageAccountType  
  }
  Return $SA
}

Function Get-AzureSubnetsFromVnetConfig
{
  param
  (
    $virtualNetwork
  )
  $subnets = @()
  Foreach ($sbnt in $virtualNetwork.subnets)
  {
    #define subnets for the virtual network
    $asbnt = New-AzureRmVirtualNetworkSubnetConfig -Name $sbnt.subnetName -AddressPrefix $sbnt.ipV4addressPrefix
    $subnets += $asbnt
  }
  return $subnets
}

Function Get-CertificatePath
{
  param
  (
    [string]$CertName,
    $Certs  
  )
  
  Write-Host $Certs.Length
  Write-Host $CertName
  $ReturnPath = ""
  Foreach ($c in $Certs)
  {
    If ($c.certificateName -eq $CertName)
    {
      Write-Host "Path to be returned" $c.certificateFilePath 
      $ReturnPath = $c.certificateFilePath
    }
  }
  Write-Host "Could not find certificate " $CertName
  Return $ReturnPath
}

Function Ensure-GatewayToSubnet
{
  param
  (
    [string]$GwName,
    [string]$RgName,
    [string]$Location,
    [string]$IpCfgName,
    $AzureSbntCfg,
    [string]$RootCertName,
    [string]$CertPath,
    $GwType, 
    $VpnType,
    $Bgp,
    $GwSku,
    $VpnClientAddrPool
  )
  
  Write-Host "Adding gateway " $GwName
  $gateway = Get-AzureRmVirtualNetworkGateway -ResourceGroupName $RgName -Name $GwName
  If (-Not($gateway))
  {
    $publicIpName = $GwName + "-PIP"
    $publicIP = New-AzureRmPublicIpAddress -Name $publicIpName -ResourceGroupName $RgName -Location $Location -AllocationMethod Dynamic
    $publicIpConfig = New-AzureRmVirtualNetworkGatewayIpConfig -Name $IpCfgName -Subnet $AzureSbntCfg -PublicIpAddress $publicIP
    
    #ensure certificate in Azure
    $rootCert = Get-AzureRmVpnClientRootCertificate -ResourceGroupName $RgName -VirtualNetworkGatewayName $GwName -VpnClientRootCertificateName $rootCertName
    If (-not($rootCert))
    {
      Write-Host "Loading gateway certificate"
      #Read root cert as base 64 string
      $bytes = [System.IO.File]::ReadAllBytes($certPath)
      $rootCertPubKeyBase64 = [Convert]::ToBase64String($bytes)

      Write-Host "Uploading certificates to Azure"
      #Upload cert to Azure
      $rootCert = New-AzureRmVpnClientRootCertificate -Name  $rootCertName -PublicCertData $rootCertPubKeyBase64  
    }
    Write-Host "Creating Virtual Gateway " $GwName
    #Create virtual network gateway
    New-AzureRmVirtualNetworkGateway -Name $GwName -ResourceGroupName $RgName -Location $Location -IpConfigurations $publicIpConfig -GatewayType $GwType -VpnType $VpnType -EnableBgp $Bgp -GatewaySku $GwSku -VpnClientAddressPool $VpnClientAddrPool -VpnClientRootCertificates $rootCert
  }
}


#load config file
$config = Get-Content $PathToConfigFile | Out-String | ConvertFrom-Json

#login and get subscription
Login-AzureRmAccount 
Get-AzureRmSubscription
Select-AzureRmSubscription -SubscriptionName $config.account.subscriptionName

#ensure resrouce group
$resourceGroup = Ensure-ResourceGroup -GroupName $config.account.resourceGroupName -Location $config.account.dataCenterName
$StorageAccountName = [Regex]::Replace($config.account.storage.accountName.ToLower(), '[^(a-z0-9)]', '')
$storageAccount = Ensure-StorageAccount -StorageAccountName $StorageAccountName -StorageAccountType $config.account.storage.type -ResourceGroupName $config.account.resourceGroupName -Location $config.account.dataCenterName

#create virtual networks
Foreach ($vnet in $config.infrastructure.virtualNetworks)
{
  $azureSubnets = Get-AzureSubnetsFromVnetConfig -VirtualNetwork $vnet
  
  Write-Host "Creating networks and subnets"  
  #Create the networks and subnets
  New-AzureRmVirtualNetwork -Name $vnet.networkName -ResourceGroupName $config.account.resourceGroupName -Location $config.account.dataCenterName -AddressPrefix $vnet.ipV4addressPrefixes -Subnet $azureSubnets -DnsServer $vnet.dnsServer
  $azureVirtualNetwork = Get-AzureRmVirtualNetwork -Name $vnet.networkName -ResourceGroupName $config.account.resourceGroupName 
  
  Write-Host "Adding gateways to subnets"
  #add gateways to subnets
  Foreach ($subnet in $vnet.subnets)
  {
    If (-Not($subnet.gateway -eq $null))
    {     
      $azureSubnetConfig = Get-AzureRmVirtualNetworkSubnetConfig -Name $subnet.subnetName -VirtualNetwork $azureVirtualNetwork
      $certPath = Get-CertificatePath -CertName $subnet.gateway.vpnRootCertificateName -Certs $config.certificates
      Write-Host "Path to certificate is" $certPath
      Ensure-GatewayToSubnet -GwName $subnet.gateway.gatewayName -RgName $config.account.resourceGroupName -Location $config.account.dataCenterName -IpCfgName $subnet.gateway.ipConfigurationName -AzureSbntCfg $azureSubnetConfig -RootCertName $subnet.gateway.vpnRootCertificateName -CertPath $certPath -GwType $subnet.gateway.gatewayType -VpnType $subnet.gateway.vpnType -Bgp $subnet.gateway.enableBGP -GwSku $subnet.gateway.gatewaySKU -VpnClientAddrPool $subnet.gateway.vpnClientAddressPool
    }
  }
  
  #Add Virtual Machines in subnets
  Foreach ($subnet in $vnet.subnets)
  {
    If ($subnet.virtualMachines)
    {
      Write-Host "Adding virtual machines to subnet " $subnet.subnetName
      #Get Subnet Index
      $foundSubnets = Get-AzureRmVirtualNetwork -Name $vnet.networkName -ResourceGroupName $config.account.resourceGroupName | Select Subnets
      $sindex = -1;
      Foreach ($fs in $foundSubnets)
      {
        $sindex = $sindex + 1;
        If ($fs -eq $subnet.subnetName)
        {
          break;
        }
      }
      Foreach ($vm in $subnet.virtualMachines)
      {
        Write-Host "Creating Virtual Machine " $vm.name
        $vmConfig = New-AzureRmVMConfig -VMName $vm.name -VMSize $vm.size
        #create nics
        $nic = @{}
        If ($vm.networkInterface.assignPublicIp)
        {
          $pip = @{}
          If ($vm.networkInterface.publicDnsName)
          {
            $pip = New-AzureRmPublicIpAddress -Name $vm.networkInterface.nicName -ResourceGroupName $config.account.resourceGroupName -DomainNameLabel $vm.networkInterface.assignPublicIp -Location $config.account.dataCenterName -AllocationMethod Dynamic -PrivateIpAddress $vm.networkInterface.ipAddress
          }
          Else
          {
            $pip = New-AzureRmPublicIpAddress -Name $vm.networkInterface.nicName -ResourceGroupName $config.account.resourceGroupName -Location $config.account.dataCenterName -AllocationMethod Dynamic  
          }
          
          $nic = New-AzureRmNetworkInterface -Name $vm.networkInterface.nicName -ResourceGroupName $config.account.resourceGroupName -Location $config.account.dataCenterName -SubnetId $vnet.Subnets[$sindex].Id -PublicIpAddressId $pip.Id -PrivateIpAddress $vm.networkInterface.ipAddress
        }
        Else
        {
          $nic = New-AzureRmNetworkInterface -Name $vm.networkInterface.nicName -ResourceGroupName $config.account.resourceGroupName -Location $config.account.dataCenterName -SubnetId $vnet.Subnets[$sindex].Id -PrivateIpAddress $vm.networkInterface.ipAddress
        }
        $PlainPassword = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($vm.localAdminCredentials.passwordEncryptedString)
        $PWord = ConvertTo-SecureString –String $PlainPassword –AsPlainText -Force
        $cred = New-Object –TypeName System.Management.Automation.PSCredential –ArgumentList $vm.localAdminCredentials.username, $PWord
        $vmConfig = Set-AzureRmVMOperatingSystem -VM $vmConfig -Windows -ComputerName $vm.name -Credential $cred -ProvisionVMAgent -EnableAutoUpdate
        $vmConfig = Set-AzureRmVMSourceImage -VM $vmConfig -PublisherName $vm.image.publisherName -Offer $vm.image.offer -Skus $vm.image.skuName -Version "latest"
        $vmConfig = Add-AzureRmVMNetworkInterface -VM $vmConfig -Id $nic.Id

        $diskName = "OSDisk"
        $osDiskUri = $storageAccount.PrimaryEndpoints.Blob.ToString() + "vhds/" + $vmName + $diskName  + ".vhd"
        $vmConfig = Set-AzureRmVMOSDisk -VM $vmConfig -Name $diskName -VhdUri $osDiskUri -CreateOption fromImage
        New-AzureRmVM -ResourceGroupName $config.account.resourceGroupName -Location $config.account.dataCenterName -VM $vmConfig
      }
    }
  }
}
